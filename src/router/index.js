import Vue from "vue";
import Router from "vue-router";
import AccountList from "@/components/AccountList";
import AccountDetail from "@/components/AccountDetail";
import {TokenService} from "../services/storage.service";
import user from "../services/user.service";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      name: "AccountList",
      component: AccountList
    },
    {
      path: "/:accountId",
      name: "AccountDetail",
      component: AccountDetail
    }
  ]
});

router.beforeEach(async (to, from, next) => {
  const isPublic = to.matched.some(record => record.meta.public)
  const onlyWhenLoggedOut = to.matched.some(record => record.meta.onlyWhenLoggedOut)
  const loggedIn = !!TokenService.getToken();

  if (!isPublic && !loggedIn) {
    await user.login();
  }

  // Do not allow user to visit login page or register page if they are logged in
  if (loggedIn && onlyWhenLoggedOut) {
    return next('/')
  }

  next();
})

export default router;
