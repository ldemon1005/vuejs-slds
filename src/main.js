// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import { store } from "./store";

import ApiService from "./services/api.service";
import {TokenService} from "./services/storage.service"

Vue.config.productionTip = false;
ApiService.init(`${process.env.VUE_APP_API_URL}/${process.env.VUE_APP_VER_API}`)

// If token exists set header
if (TokenService.getToken()) {
  ApiService.setHeader()
}

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  template: "<App/>",
  components: { App }
});
