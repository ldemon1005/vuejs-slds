'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  VUE_APP_API_URL: '"http://183.91.11.56:9001"',
  VUE_APP_VER_API:'"v46.0"',
  VUE_APP_CLIENT_ID:'"3MVG9iLRabl2Tf4gr86vkEZj9L.vd8vrhn9RTezhzKRTl9MBN241HKK2THi_us3O5d9FNQXwVotI1Zg1lRuFp"',
  VUE_APP_CLIENT_SECRET:'"B63176167C95106AAB70F2A7A4711B4C4DD29A8106E738AA429BD7437D788F8D"'
})
